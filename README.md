# Ontology Service

Image tags are in Container registry https://gitlab.com/kontrolon-2.0/ontology-service/container_registry
Please note parameter RDF4J_HOST_ADDRESS. It is used to connect to rdf4j server that is also within the docker image. Parameter should be configured in this way:
RDF4J_HOST_ADDRESS=http://{container_name}:{container port}
Example: RDF4J_HOST_ADDRESS=http://ontologyservice:8080
This setup in below should also work, if docker runs on localhost (or put IP if static): RDF4J_HOST_ADDRESS=http://localhost:8080

Docker up: docker-compose up -d


Docker stop: docker-compose stop


Docker restart: docker-compose start


Docker stop and remove the containers: docker-compose down


Docker stop, remove the containers and the volumes: docker-compose down -v


Or run by docker
docker run -d -p 127.0.0.1:8080:8080 -e JAVA_OPTS="-Xms1g -Xmx2g" -e RDF4J_HOST_ADDRESS=http://127.0.0.1:8080 -v data:/var/rdf4j -v logs:/usr/local/tomcat/logs emacontrol-ontology-infrastructure:{tag}
When docker is up on localhost you can access to a Swagger API doc: http://localhost:8080/semantic-registry/swagger-ui.html
To access to the Mode Editor: http://localhost:8080/mode  login with: admin/adminqw

